import { createAsyncThunk } from "@reduxjs/toolkit";
import authServices from "../../services/auth.services";

export const loginThunk = createAsyncThunk(
    'loginThunk',
    async (input) => {        
        try{
            const x = await authServices.login(input)    
            // console.log('berhasil')        
            return x       
        }catch(err){            
            // console.log('gagal')        
            return err         
        }
    }
)