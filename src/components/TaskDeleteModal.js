import { Modal, Button } from 'react-bootstrap';
import taskServices from '../services/task.services';

function TaskDeleteModal({show,onClose,task,refresh}) {
    const deleteTask = async () => {
        await taskServices.delete(task.id)
        onClose()
        refresh()
    }
    return (
        <Modal className="" show={show} onClose={onClose}>            
                <Modal.Header >
                    <Modal.Title>Delete Confirmation</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <p>Are you sure want to delete this task ?</p>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="danger" onClick={onClose}>No</Button>
                    <Button variant="primary" onClick={deleteTask}>Yes</Button>
                </Modal.Footer>            
        </Modal>
    );
}
export default TaskDeleteModal;  