import { Modal, Button } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { logout } from '../store/reducers/LoginReducer';


function LogoutModal({show,onClose}) {
    const dispatch = useDispatch()
    const yesOption = (e) => {
        e.preventDefault()
        dispatch(logout())
    }
    return (
        <Modal className="" show={show} onClose={onClose}>            
                <Modal.Header >
                    <Modal.Title>Logout Confirmation</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <p>Are you sure want to logout ?</p>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="danger" onClick={onClose}>No</Button>
                    <Button variant="primary" onClick={yesOption}>Yes</Button>
                </Modal.Footer>            
        </Modal>
    );
}
export default LogoutModal;  