import React from 'react'

function DateBox({ date , clickFunc , selectedDate}) {
    return (
        <button className={(selectedDate == date?.fullDate) ? 'bg-dark border border-white text-white' : 'bg-secondary border border-white text-white' }
        style={{ width: '140px', height: '140px' , transform: 'rotate(90deg)'}}
        onClick={(e) => clickFunc(date?.fullDate)}>
            <h6 className='bg-primary m-0'>{date?.day.slice(0, 3)}</h6>
            <h6 className='m-0' style={{ fontSize:'75px' }}>{date?.date}</h6>
            <h6 className='m-0' style={{ fontSize:'12px' }}>{date?.month.slice(0, 3) + '-' + date?.year}</h6>
        </button>
    )
}

export default DateBox