import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Login from './pages/Login';
import Main from './pages/Main';
import ExampleRedux from './pages/ExampleRedux';
import './App.css'

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path='/' element={<Main />} />
          <Route path='/login' element={<Login />} />   
          <Route path='/redux' element={<ExampleRedux />} />   
        </Routes>
      </Router>
    </div>
  );
}

export default App;
