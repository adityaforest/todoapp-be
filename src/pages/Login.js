import React, { useEffect, useState } from 'react'
import styles from './Login.module.css'
import { useDispatch, useSelector } from 'react-redux'
import { loginThunk } from '../store/thunk/LoginThunk'
import {useNavigate} from 'react-router-dom'

function Login() {
  const [uniqueNumber, setUniqueNumber] = useState('')

  const dispatch = useDispatch()
  const isLoading = useSelector(state => state.LoginReducer.loading)
  const errorMessage = useSelector(state => state.LoginReducer.errorMessage)
  const loggedIn = useSelector(state => state.LoginReducer.loggedIn)

  const navigate = useNavigate()

  useEffect(() => {
    console.log(loggedIn)
    if(loggedIn) navigate('/')
  }, [loggedIn])
  

  const handleUNChange = (e) => {
    setUniqueNumber(e.target.value.slice(0, 4))
  }

  const onSubmitLogin = (e) => {
    e.preventDefault()
    dispatch(loginThunk(uniqueNumber))
  }

  return (
    <div className={styles.Login}>
      {isLoading ?
        <div style={{ width: '100%', height: '100%', backgroundColor: 'rgba(0,0,0,0.5)', zIndex: '100', position: 'absolute' }}
          className='d-flex justify-content-center align-items-center'>
          <img src="loading.gif" alt="" />
        </div>
        :
        null
      }
      <div className={styles.LoginBox}>
        <h1 style={{ margin: '5px' }}>To Do List App</h1>
        <h5 style={{ margin: '0' }}>Created for Binar Academy Mock Tech Test</h5>
        <h6 style={{ margin: '0' }}>By. Aditya Forest Resananta</h6>
        <form onSubmit={onSubmitLogin} className='d-flex align-items-center justify-content-center mt-5'>
          <input type="number" placeholder='Enter your unique number ...'
            value={uniqueNumber} onChange={handleUNChange} className={styles.input} />
          <button className='bg-primary rounded text-white mx-2' style={{ height: '30px', width: '100px' }}>Login</button>
        </form>
        <h6 className='text-danger'>{errorMessage}</h6>
        <div className='mt-5' style={{ width: '300px' }}>
          <h6>Unique Numbers for test purpose :</h6>
          <p style={{ margin: '0' }}>User A : 1234</p>
          <p style={{ margin: '0' }}>User B : 5678</p>
        </div>
        <div className='mt-2' style={{ width: '300px' }}>
          <h6>Stack used for this app :</h6>
          <p className='font-weight-bold'>React , Redux , Bootstrap , NodeJS(Express) , Sequelize , PostgreSQL</p>
        </div>
      </div>

    </div>
  )
}

export default Login