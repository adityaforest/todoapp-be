import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { exampleThunk } from '../store/thunk/ExampleThunk'
import { increaseCounter, decreaseCounter, setCounter } from '../store/reducers/ExampleReducer'

function ExampleRedux() {
    const [input,setInput] = useState(0)

    const exampleReducerState = useSelector(state => state.ExampleReducer)
    const dispatch = useDispatch()

    const handleInputChange = (e) => {
        setInput(e.target.value)
    }

    const increaseCounterReducer = (e) => {
        dispatch(increaseCounter())
    }

    const decreaseCounterReducer = (e) => {
        dispatch(decreaseCounter())
    }

    const setCounterReducer = (e) => {
        dispatch(setCounter(input))
    }

    const setCounterThunk = (e) => {
        dispatch(exampleThunk(input))
    }

    return (
        <div>
            <h1>{exampleReducerState.counter}</h1>
            <button onClick={increaseCounterReducer}>INCREASE COUNTER VIA REDUCER</button>
            <button onClick={decreaseCounterReducer}>DECREASE COUNTER VIA REDUCER</button>                        
            <input type="number" value={input} onChange={handleInputChange}/>
            <button onClick={setCounterReducer}>SET COUNTER VIA REDUCER</button>
            <button onClick={setCounterThunk}>SET COUNTER VIA THUNK</button>
        </div>
    )
}

export default ExampleRedux